package com.levelup.example;


import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.util.List;

@Getter
@Builder
@Setter
@EqualsAndHashCode(exclude = "bids")
@ToString
public class User {
    private final String firstName;
    private final String lastName;
    private List<Bid> bids;

}
