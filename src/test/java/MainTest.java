import com.levelup.example.Bid;
import com.levelup.example.User;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.junit.Assert.assertEquals;

@RunWith(MockitoJUnitRunner.class)
public class MainTest {

    @Test
    public void test() {

        User user = User.builder().firstName("firstname").lastName("lastname").build();
        User user2 = User.builder().firstName("firstname").lastName("lastname").build();

        List<Bid> bids1 = new ArrayList<Bid>();

        bids1.add(new Bid(10, user));
        bids1.add(new Bid(20, user));

        List<Bid> bids2 = new ArrayList<Bid>();

        bids2.add(new Bid(10, user2));
        bids2.add(new Bid(20, user2));

        user.setBids(bids1);
        user2.setBids(bids2);
        System.out.println(user);
        assertEquals(user,user2);
    }

}
